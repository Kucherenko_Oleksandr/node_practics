import * as utils_ok from 'test-utils';
import utils from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const promises = utils.promisify(utils_ok.runMePlease);

try {
    const result = await promises(params);
    console.log(result);
} catch (error) {
    console.log(error);
}
